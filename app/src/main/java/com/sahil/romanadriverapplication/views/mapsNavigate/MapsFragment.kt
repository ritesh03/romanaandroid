package com.sahil.romanadriverapplication.views.mapsNavigate

import android.location.Location
import androidx.fragment.app.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.sahil.romanadriverapplication.R
import com.sahil.romanadriverapplication.databinding.FragmentMapsBinding
import com.sahil.romanadriverapplication.utils.AppConstants
import com.sahil.romanadriverapplication.utils.Prefs
import com.sahil.romanadriverapplication.webservices.response.tripsApiResponse.TripData

class MapsFragment : Fragment(),LocationListener,OnMapReadyCallback {
    private lateinit var prefs: Prefs
    private lateinit var binding:FragmentMapsBinding
    private lateinit var mLocationRequest:LocationRequest
    private lateinit var mGoogleApiClient: GoogleApiClient

    private val INTERVAL = (1000 * 10).toLong()
    private val FASTEST_INTERVAL = (1000 * 5).toLong()


    protected fun createLocationRequest(){
        mLocationRequest = LocationRequest.create().apply {
            interval = INTERVAL
            fastestInterval = FASTEST_INTERVAL
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }


    }

    private val callback = OnMapReadyCallback { googleMap ->
        val sydney = LatLng(-34.0, 151.0)
        googleMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMapsBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        setData()
        setUpMap()


    }
    fun init(){
        prefs = Prefs.with(requireContext())
    }
    fun setData(){
       var data= prefs.getObject(AppConstants.SELETED_TRIP_DATA,TripData::class.java)
        binding.tvPassengerName.text = data.passengerName.firstName + " "+ data.passengerName.lastName
    }
    fun setUpMap(){
        createLocationRequest()
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    override fun onLocationChanged(location: Location) {
    }

    override fun onMapReady(googlemap: GoogleMap) {
    }


}