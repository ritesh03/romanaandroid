package com.sahil.romanadriverapplication.views.trips

import android.util.Log
import com.google.gson.Gson
import com.sahil.romanadriverapplication.utils.AppConstants
import com.sahil.romanadriverapplication.webservices.response.createLocationResponse.CreateLocationApiResponse
import com.sahil.romanadriverapplication.webservices.response.tripsApiResponse.TripsApiResponse
import com.sahil.romanadriverapplication.webservices.retrofit.RetofitClient
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException


class TripsPresenter:TripsContract.Presenter{
    private  val TAG = "TripsPresenter"

    private var view: TripsContract.View? = null
    override fun performTripsApiCall(driverId: String, date: String,accessToken: String) {

        view!!.showLoading()
       val map =HashMap<String,Any>()
        map.put("uniquieAppKey",AppConstants.APP_UNIQUE_KEY)
        map.put("driverId",driverId)
        map.put("startDate",date)
        map.put("tripStatus",0)
        Log.d(TAG, "performTripsApiCall: params:-- ${Gson().toJson(map)} access token :-- $accessToken")
        RetofitClient.get().apiAllListTrips(accessToken, map = map).enqueue(object :Callback<TripsApiResponse>{
            override fun onResponse(call: Call<TripsApiResponse>, response: Response<TripsApiResponse>) {
                Log.d(TAG, "onResponse: TripsResponsecode --- ${response.code()}")

                if(response.isSuccessful && response.code() == 200){
                    Log.d(TAG, "onResponse: response body:- ${Gson().toJson(response.body())}")
                    response.body()?.let { view?.apiSuccess(it) }
                    view!!.dismissLoading()
                }
                else if(response.code() == AppConstants.UnAuthorized){
                    view!!.sessionExpired()
                }
                else{
                    try {
                        view?.tripsError(JSONObject(response.errorBody()!!.string()).getString("message"))
                        view!!.dismissLoading()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        view!!.dismissLoading()
                    } catch (e: IOException) {
                        e.printStackTrace()
                        view!!.dismissLoading()
                    }
                }
            }
            override fun onFailure(call: Call<TripsApiResponse>, t: Throwable) {
                Log.d(TAG, "onFailure: ${t.message}")
                view!!.tripsFailure(t.message.toString())
                view!!.dismissLoading()
            }
        })
    }

    override fun attachView(view: TripsContract.View) {
        this.view = view

    }

    override fun performCreateLocation(driverId: String, latitude: Double, longitude: Double, accessToken: String) {
        val map:HashMap<String,Any> = HashMap()
        map.put("uniquieAppKey",AppConstants.APP_UNIQUE_KEY)
        map.put("Id",driverId)
        map.put("lat",latitude)
        map.put("long",longitude)
        RetofitClient.get().apiCreateDriverLocation(accessToken,map).enqueue(object: Callback<CreateLocationApiResponse>{
            override fun onResponse(call: Call<CreateLocationApiResponse>, response: Response<CreateLocationApiResponse>) {
                Log.d(TAG, "onResponse: perform create location responnse code :--  ${response.code()}")
                if(response.isSuccessful){
                    Log.d(TAG, "onResponse:perform create location responnse body :-  ${response.body()}")
                    response.body()?.let { view?.apicreateLocationApiSuccess(it) }

                }
                else if(response.code() == AppConstants.UnAuthorized){
                   view?.sessionExpired()
                }else{
                    try {
                        view?.apiCreteLocationError(JSONObject(response.errorBody()!!.string()).getString("message"))
                        view!!.dismissLoading()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        view!!.dismissLoading()
                    } catch (e: IOException) {
                        e.printStackTrace()
                        view!!.dismissLoading()
                    }
                }
            }

            override fun onFailure(call: Call<CreateLocationApiResponse>, t: Throwable) {
                Log.d(TAG, "onFailure: perform create location:-   ${t.message}")
                view?.apiCreateLocationFaliure(t.message.toString())
            }

        })
    }

    override fun detachView() {

    }
}