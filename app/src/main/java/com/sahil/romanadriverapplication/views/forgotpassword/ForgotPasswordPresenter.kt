package com.sahil.romanadriverapplication.views.forgotpassword

import android.util.Log
import com.sahil.romanadriverapplication.utils.AppConstants
import com.sahil.romanadriverapplication.webservices.retrofit.RetofitClient
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class ForgotPasswordPresenter :ForgotPasswordContract.Presenter {
    private  val TAG = "ForgotPasswordPresenter"

    private var view: ForgotPasswordContract.View? = null

    override fun performForgotPassword(email: String) {
        view!!.showLoading()

        val map:HashMap<String,String> = HashMap()
        map.put("uniquieAppKey",AppConstants.APP_UNIQUE_KEY)
        map.put("email",email)
        Log.d(TAG, "performForgotPassword: ")
       RetofitClient.get().apiForgotPassword(map).enqueue(object :Callback<ResponseBody>{
           override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
             if(response.isSuccessful){
                   view!!.forgotPasswordSuccess("Email Sent!!")
                 view!!.dismissLoading()
             }
               else{
                 Log.d(TAG, "onResponse: ${response.errorBody()}")
                 Log.d(TAG, "onResponse: ${JSONObject(response.errorBody()!!.string()).getString("message")}")
                  //   val error:String = JSONObject(response.errorBody()!!.string()).getString("message")

                 view?.forgotPasswordError("The email you have entered has not been registered.")



             }
           }

           override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
              view?.forgotPasswordFailure(t.message.toString())
               view?.dismissLoading()
           }

       })


    }

    override fun attachView(view: ForgotPasswordContract.View) {
        this.view = view
    }

    override fun detachView() {

    }

}