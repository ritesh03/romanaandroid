package com.sahil.romanadriverapplication.views.login

import com.sahil.romanadriverapplication.webservices.response.loginApiResponse.LoginResponse

class LogInContract {

    interface View {




        fun loginSuccess(data: LoginResponse)



        fun loginError(errorMessage: String)

        fun loginFailure( failureMessage:String)

        fun showLoading()

        fun dismissLoading()

        fun sessionExpired()
    }

    interface Presenter {
        fun performLogin(email: String, password: String)

        fun attachView(view: View)

        fun detachView()

    }
}