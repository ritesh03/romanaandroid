package com.sahil.romanadriverapplication.views.tripDetail

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.*
import android.provider.Settings
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.sahil.romanadriverapplication.R
import com.sahil.romanadriverapplication.utils.GeocodingLocation
import com.sahil.romanadriverapplication.databinding.FragmentTripDetailBinding
import com.sahil.romanadriverapplication.utils.AppConstants
import com.sahil.romanadriverapplication.utils.GeneralMethods
import com.sahil.romanadriverapplication.utils.Prefs
import com.sahil.romanadriverapplication.views.MainActivity
import com.sahil.romanadriverapplication.webservices.response.addnotesresponse.AddNotesResponse
import com.sahil.romanadriverapplication.webservices.response.createLocationResponse.CreateLocationApiResponse
import com.sahil.romanadriverapplication.webservices.response.tripsApiResponse.TripData
import com.sahil.romanadriverapplication.webservices.response.tripupadteresponse.TripUpdateApiResponse

class TripDetailFragment : Fragment(),View.OnClickListener,TripDetailContract.View {

 lateinit var binding:FragmentTripDetailBinding
    private  val TAG = "TripDetailFragment"
 lateinit var prefs:Prefs
    var uri = "";
    var latLong:String = ""
    var tripStatuss = 0
    private lateinit var tripNotes:String
    private lateinit var tripId:String
    lateinit var presenter: TripDetailContract.Presenter
   var degistinationAddress:String = ""
    var degistaionPhonenumber:String = ""
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentTripDetailBinding.inflate(inflater,container,false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setData()
        setClickListners()
    }
    fun init(){
        prefs = Prefs.with(context)
        presenter = TripDetailPresenter()
        presenter.attachView(this)
        checkAndRequestPermissions()
    }
    fun setData(){
        binding.toolbar.toolbar.setNavigationIcon(resources.getDrawable(R.drawable.ic_baseline_arrow_back_24))
        val data = prefs.getObject(AppConstants.SELETED_TRIP_DATA, TripData::class.java)
        Log.d(TAG, "setData: seleted data:- ${ prefs.getObject(AppConstants.SELETED_TRIP_DATA, TripData::class.java)}")
        Log.d(TAG, "setData: TripRequest Data :---> ${data.tripRequest}")
        if(data.OriginAddress.comments != ""){
            binding.tvCommentsDescription.text = data.OriginAddress.comments
        }else{
            binding.tvCommentsDescription.text = getString(R.string.no_notes)
        }
        if(binding.tvNotesDescription.text.isNotEmpty()){
            binding.tvAddNote.visibility = View.GONE
            binding.ivEditNotesDesc.visibility = View.VISIBLE
        }
        if(data.DestinationAddress.phoneNo != ""){
            degistaionPhonenumber = data.DestinationAddress.phoneNo
        }

        when(data.tripRequest){
            AppConstants.PICKUP->{
                binding.toolbar.title.text = data.tripTime+ " "+ getString(R.string.pick_up)
            }
            AppConstants.DROPOFF->{
                binding.toolbar.title.text = data.tripTime+ " "+getString(R.string.drop_off)
            }
            AppConstants.WILLCALL->{
                binding.toolbar.title.text = data.tripTime+ " "+getString(R.string.will_call)
            }
        }
        if(data.confirmationNumber != "" ) {
            binding.tvConformationNumber.text = "Confirmation number: "+data.confirmationNumber.toString()
        } else{
            binding.tvConformationNumber.visibility = View.GONE
        }
        binding.tvPassengerName.text = data.passengerName.firstName+" "+data.passengerName.lastName
        binding.tvAddressDescription.text = data.OriginAddress.streetAddress+ ", "+data.OriginAddress.city+", "+data.OriginAddress.country+", "+data.OriginAddress.postalCode+"."
        if(data.Notes.isNotEmpty()) {
            binding.tvNotesDescription.text = data.Notes[data.Notes.size - 1].note
        }
        Log.d(TAG, "setData: phone number ${data.OriginAddress.phoneNo}")
        if(data.OriginAddress.phoneNo != "" ){
            uri =  "tel:" + data.OriginAddress.phoneNo.toString()
            binding.ivPhone.visibility = View.VISIBLE
        }
        else{
            binding.ivPhone.visibility = View.GONE
        }
       val navControlle = findNavController();
        navControlle.currentBackStackEntry?.savedStateHandle?.getLiveData<String>("addNoteRes")?.observe(
            viewLifecycleOwner) {result->
            Log.d(TAG, "setData: live result ${Gson().toJson(result)}")
            presenter.performCreateNotes(prefs.getString(AppConstants.DRIVER_ID,""),
            data._id,
            result,
            AppConstants.ADMIN_ACCESS_TOKEN)
        }
        tripId = data._id
        degistinationAddress = data.DestinationAddress.streetAddress+ ", "+data.DestinationAddress.city+", "+data.DestinationAddress.country+", "+data.DestinationAddress.postalCode+"."

        when(data.tripStatus){
            AppConstants.DRIVER_ON_ROUTE->{
                binding.btnSubmit.text = "Picked Up"
                if(data.DestinationAddress.comments != ""){
                    binding.tvCommentsDescription.text = data.DestinationAddress.comments
                }else{
                    binding.tvCommentsDescription.text = getString(R.string.no_notes)
                }
                binding.tvAddressDescription.text = data.DestinationAddress.streetAddress+ ", "+data.DestinationAddress.city+", "+data.DestinationAddress.country+", "+data.DestinationAddress.postalCode+"."

                if(data.DestinationAddress.phoneNo != "" ){
                    uri =  "tel:" + data.DestinationAddress.phoneNo.toString()
                    binding.ivPhone.visibility  = View.VISIBLE
                }
                else{
                    binding.ivPhone.visibility = View.GONE
                }
                tripStatuss = AppConstants.DRIVER_ON_ROUTE
            }
            AppConstants.UNSCHEDULED->{
                tripStatuss = AppConstants.UNSCHEDULED
            }
            AppConstants.SCHEDULED->{
                binding.btnSubmit.text = "On Route"
                tripStatuss = AppConstants.SCHEDULED
            }
            AppConstants.PICKEDUP->{
                binding.btnSubmit.text = "Delivered"
               tripStatuss = AppConstants.PICKEDUP
            }
            AppConstants.DELIVERED->{
                tripStatuss = AppConstants.DELIVERED
            }
            AppConstants.CANCELED->{
                tripStatuss = AppConstants.CANCELED
            }
            AppConstants.NO_SHOW->{
                tripStatuss = AppConstants.NO_SHOW
            }
            AppConstants.MISSED->{
                tripStatuss = AppConstants.MISSED
            }
            AppConstants.REROUTE->{
                tripStatuss = AppConstants.REROUTE
            }
        }
        val locationAddress = GeocodingLocation
        locationAddress.getAddressFromLocation(binding.tvAddressDescription.text.toString(),requireContext().applicationContext, GeocoderHandler())
    }
    fun callTripUpdateApi(tripStatus:Int){
        presenter.performTripUpadte(prefs.getString(AppConstants.DRIVER_ID,""),tripId,tripStatus,AppConstants.ADMIN_ACCESS_TOKEN)
    }
    fun setClickListners(){
        binding.toolbar.toolbar.setNavigationOnClickListener {
        startActivity(Intent(context,MainActivity::class.java))
        }
        binding.ivNavigation.setOnClickListener(this)
        binding.ivPhone.setOnClickListener(this)
        binding.tvAddNote.setOnClickListener(this)
        binding.tvNotesDescription.setOnClickListener(this)
        binding.btnSubmit.setOnClickListener(this)
        binding.ivEditNotesDesc.setOnClickListener(this)
    }
    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.ivPhone->{
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse(uri)
                startActivity(intent)
            }
            R.id.tvAddNote->{
                startAddNoteFragment()
            }
            R.id.ivEditNotesDesc->{
                startAddNoteFragment()
            }
            R.id.tvNotesDescription->{
//                binding.tvAddNote.visibility = View.GONE
                startAddNoteFragment()
            }
            R.id.btnSubmit->{
                when(tripStatuss){
                    AppConstants.SCHEDULED->{
                        callTripUpdateApi(AppConstants.DRIVER_ON_ROUTE)
                    }
                    AppConstants.DRIVER_ON_ROUTE->{
                        callTripUpdateApi(AppConstants.PICKEDUP)
                    }
                    AppConstants.PICKEDUP->{
                        callTripUpdateApi(AppConstants.DELIVERED)
                    }
                }
            }
            R.id.ivNavigation->{
               if(latLong != "") {
                   Log.d(TAG, "onClick: latitude ${latLong}")
                   val address = "google.navigation:q=" + latLong + "&mode=l";
                   val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse(address));
                   mapIntent.setPackage("com.google.android.apps.maps");
                   if (mapIntent.resolveActivity(requireContext().packageManager) != null) {
                       startActivity(mapIntent);
                   } else {
                       Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
                   }
               }
            }
        }
    }
    private fun startAddNoteFragment(){
        tripNotes = binding.tvNotesDescription.text.toString()
        val action = TripDetailFragmentDirections.actionTripDetailFragmentToAddNoteFragment(tripNotes)
        findNavController().navigate(action)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            TripDetailFragment().apply {
                arguments = Bundle().apply {}
            }
    }

    override fun apiSuccess(data: AddNotesResponse) {
        Toast.makeText(context,"Note created!!",Toast.LENGTH_LONG).show()
        binding.tvNotesDescription.text = data.data.data.Notes[data.data.data.Notes.size-1].note
        prefs.remove(AppConstants.NOTES_DATA)
    }
    override fun apiError(error: String) {
        GeneralMethods.apiErrorAlertDialog(requireContext(),error)
    }
    override fun apiFaliure(faliure: String) {
        GeneralMethods.apiErrorAlertDialog(requireContext(),getString(R.string.connection_error))
    }
    override fun apiTripUpdateSuccess(data: TripUpdateApiResponse) {
        Log.d(TAG, "apiTripUpdateSuccess: ${Gson().toJson(data)}")
       when(data.data.tripStatus){
           AppConstants.DRIVER_ON_ROUTE->{
               binding.btnSubmit.text = "Picked Up"
               tripStatuss = AppConstants.DRIVER_ON_ROUTE
           }
           AppConstants.UNSCHEDULED->{
               tripStatuss = AppConstants.UNSCHEDULED
           }
           AppConstants.SCHEDULED->{
               binding.btnSubmit.text = "On Route"
               tripStatuss = AppConstants.SCHEDULED
           }
           AppConstants.PICKEDUP->{
               binding.btnSubmit.text = "Delivered"
               tripStatuss = AppConstants.PICKEDUP
               binding.tvAddressDescription.text = degistinationAddress
               if(degistaionPhonenumber != "" ){
                   uri =  "tel:" + degistaionPhonenumber.toString()
                   binding.ivPhone.visibility = View.VISIBLE
               }
               else{
                   binding.ivPhone.visibility = View.GONE
               }

               val locationAddress = GeocodingLocation
               locationAddress.getAddressFromLocation(binding.tvAddressDescription.text.toString(),requireContext().applicationContext, GeocoderHandler())
           }
           AppConstants.DELIVERED->{
              deliverdDialog("Thank you for the trip")
               tripStatuss = AppConstants.DELIVERED
           }
           AppConstants.CANCELED->{
               tripStatuss = AppConstants.CANCELED
           }
           AppConstants.NO_SHOW->{
               tripStatuss = AppConstants.NO_SHOW
           }
           AppConstants.MISSED->{
               tripStatuss = AppConstants.MISSED
           }
           AppConstants.REROUTE->{
               tripStatuss = AppConstants.REROUTE
           }
       }

    }

    fun deliverdDialog( message:String) {
        androidx.appcompat.app.AlertDialog.Builder(requireContext())
            .setMessage(message)
            .setPositiveButton(android.R.string.ok) { dialogInterface, i ->
                startActivity(Intent(requireContext(),MainActivity::class.java))
                dialogInterface.dismiss()
            }
            .setCancelable(false)
            .show()
    }    override fun apiTripUpdateError(error: String) {
        GeneralMethods.apiErrorAlertDialog(requireContext(),error.toString())
    }

    override fun apitripUpdateFaliure(faliure: String) {
        GeneralMethods.apiErrorAlertDialog(requireContext(),getString(R.string.connection_error))
    }


    override fun sessionExpire() {
        GeneralMethods.tokenExpired(requireContext(),getString(R.string.session_expire))
    }

    override fun showLoading() {
        binding.progressBar.visibility = View.VISIBLE
    }

    override fun dismissLoading() {
        binding.progressBar.visibility = View.GONE
    }

   inner class GeocoderHandler : Handler() {
        private  val TAG = "TripDetailFragment"
        override fun handleMessage(message: Message) {
            val locationAddress: String?
            locationAddress = when (message.what) {
                1 -> {
                    val bundle: Bundle = message.getData()
                    bundle.getString("address")
                }
                else -> null
            }
            Log.d(TAG, "handleMessage: ${locationAddress.toString().trim()}")
            latLong = locationAddress.toString().trim()

        }
    }

    private val REQUEST_ID_MULTIPLE_PERMISSIONS = 3

    private fun checkAndRequestPermissions(): Boolean {
        val camerapermission = ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
        val writepermission = ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
       val listPermissionsNeeded = ArrayList<String>()

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(requireActivity(), listPermissionsNeeded.toTypedArray(), REQUEST_ID_MULTIPLE_PERMISSIONS)
            return false
        } else {

        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_ID_MULTIPLE_PERMISSIONS -> {

                val perms = java.util.HashMap<String, Int>()
                // Initialize the map with both permissions
                perms[Manifest.permission.ACCESS_COARSE_LOCATION] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.ACCESS_FINE_LOCATION] = PackageManager.PERMISSION_GRANTED
                // Fill with actual results from user
                if (grantResults.size > 0) {
                    for (i in permissions.indices) perms[permissions[i]] = grantResults[i]
                    // Check for both permissions
                    if (perms[Manifest.permission.ACCESS_FINE_LOCATION] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.ACCESS_FINE_LOCATION] == PackageManager.PERMISSION_GRANTED
                    ) {

                    }
                    else {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(
                                requireActivity(),
                                Manifest.permission.ACCESS_FINE_LOCATION
                            )
                            || ActivityCompat.shouldShowRequestPermissionRationale(
                                requireActivity(),
                                Manifest.permission.ACCESS_COARSE_LOCATION
                            )
                        ) {
                            showDialogOK("Service Permissions are required for this app",
                                DialogInterface.OnClickListener { dialog, which ->
                                    when (which) {
                                        DialogInterface.BUTTON_POSITIVE -> checkAndRequestPermissions()
                                        DialogInterface.BUTTON_NEGATIVE ->
                                            // proceed with logic by disabling the related features or quit the app.
                                            //  finish()

                                            Log.e(TAG,"required")
                                        // Toast.makeText(requireActivity(), "Required", Toast.LENGTH_LONG).show()
                                    }
                                })
                        } else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?")
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }//permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                    }
                } else {
                    if (checkAndRequestPermissions()) {
                    }
                }
            }
        }
    }


    private fun showDialogOK(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(requireActivity())
            .setMessage(message)
            .setPositiveButton("OK", okListener)
            .setNegativeButton("Cancel", okListener)
            .create()
            .show()
    }
    private fun explain(msg: String) {
        val dialog = androidx.appcompat.app.AlertDialog.Builder(requireActivity())
        dialog.setMessage(msg)
            .setPositiveButton("Yes") { paramDialogInterface, paramInt ->
                //  permissionsclass.requestPermission(type,code);
                startActivity(
                    Intent(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:com.sahil.romanadriverapplication")
                    )
                )
            }
            .setNegativeButton("Cancel") { paramDialogInterface, paramInt ->
                Toast.makeText(requireActivity(), "Required", Toast.LENGTH_LONG).show()
            }
        dialog.show()
    }


}