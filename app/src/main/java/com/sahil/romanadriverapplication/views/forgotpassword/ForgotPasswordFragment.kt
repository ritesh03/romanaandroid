package com.sahil.romanadriverapplication.views.forgotpassword

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.sahil.romanadriverapplication.R
import com.sahil.romanadriverapplication.databinding.FragmentForgotPasswordBinding
import com.sahil.romanadriverapplication.utils.GeneralMethods
import android.text.TextUtils
import android.util.Patterns


class ForgotPasswordFragment : Fragment(),View.OnClickListener,ForgotPasswordContract.View {
    lateinit var binding:FragmentForgotPasswordBinding
    lateinit var presenter: ForgotPasswordContract.Presenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentForgotPasswordBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        clickListners()
    }

    fun init(){
        presenter = ForgotPasswordPresenter()
        presenter.attachView(this)
    }
    fun clickListners(){
          binding.ivBack.setOnClickListener(this)
        binding.tvSend.setOnClickListener(this)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            ForgotPasswordFragment().apply {
                arguments = Bundle().apply {    }
            }
    }

    override fun onClick(v: View?) {
       when(v!!.id){
           R.id.ivBack->{
               binding.ivBack.findNavController().navigate(R.id.action_forgotPasswordFragment_to_logInFragment)
           }
           R.id.tvSend->{
               if(checkvalidity()) {
                   presenter.performForgotPassword(binding.etEmail.text.toString().trim())
               }
           }

       }
    }

    fun checkvalidity():Boolean{

        if(binding.etEmail.text.isNullOrEmpty()){
            GeneralMethods.showSnackBar(requireContext(),binding.parent,getString(R.string.enter_email))
            return false
        }
        else if(!isValidEmail(binding.etEmail.text.toString().trim())){
            GeneralMethods.showSnackBar(requireContext(),binding.parent,getString(R.string.enter_valid_email))

            return false
        }
        else{
            return true
        }
    }
    fun isValidEmail(target: CharSequence?): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    override fun forgotPasswordSuccess(success: String) {
      GeneralMethods.apiErrorAlertDialog(requireContext(),success)
    }

    override fun forgotPasswordError(errorMessage: String) {
        GeneralMethods.apiErrorAlertDialog(requireContext(),errorMessage)
    }

    override fun forgotPasswordFailure(failureMessage: String) {
        GeneralMethods.apiErrorAlertDialog(requireContext(),getString(R.string.connection_error))

    }

    override fun showLoading() {
      binding.progressBar.visibility = View.VISIBLE
    }

    override fun dismissLoading() {
       binding.progressBar.visibility = View.GONE
    }

    override fun sessionExpired() {
      GeneralMethods.tokenExpired(requireContext(),getString(R.string.session_expire))
    }


}