package com.sahil.romanadriverapplication.views.changePassword

import com.sahil.romanadriverapplication.views.login.LogInContract
import com.sahil.romanadriverapplication.webservices.response.updatePasswordResponse.UpdatePasswordResponse

class ChangePasswordContract {

    interface View{
        fun apiSuccess(data: UpdatePasswordResponse)
        fun apiError(error:String)
        fun apiFaliure(faliure:String)
        fun sessionExpire()
        fun showLoading()
        fun dismissLoading()
    }
    interface Presenter {
        fun performChangePassword(oldPassword:String, password:String,driverId: String,accessToken:String)
        fun attachView(view: ChangePasswordContract.View)
        fun detachView()

    }
}