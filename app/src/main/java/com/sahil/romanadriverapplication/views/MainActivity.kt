package com.sahil.romanadriverapplication.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import com.sahil.romanadriverapplication.databinding.ActivityMainBinding

import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.*

import com.google.android.material.navigation.NavigationView
import com.sahil.romanadriverapplication.R
import com.sahil.romanadriverapplication.utils.AppConstants
import com.sahil.romanadriverapplication.utils.GeneralMethods
import com.sahil.romanadriverapplication.utils.Prefs

class MainActivity : AppCompatActivity(),NavigationView.OnNavigationItemSelectedListener {

    private val TAG = "MainActivity"
    lateinit var binding: ActivityMainBinding
    lateinit var navController: NavController
    var mBackPressed = 0L
    private val TIME_INTERVAL = 3000


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
        setDrawer()
        setData()
    }

    fun init(){
        binding = DataBindingUtil.setContentView(this@MainActivity,R.layout.activity_main)

    }
    fun setDrawer(){
        var toogle = ActionBarDrawerToggle(this,binding.drawerLayout,binding.toolbar.toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close)
        toogle.drawerArrowDrawable.color = resources.getColor(R.color.white)
        binding.drawerLayout.addDrawerListener(toogle)
        toogle.syncState()

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
        navController = navHostFragment.findNavController()
        binding.navView.setupWithNavController(navController)
        val header:View = binding.navView.getHeaderView(0)
        header.findViewById<TextView>(R.id.tvuserName).text = Prefs.with(this@MainActivity).getString(AppConstants.USER_NAME,"")
        header.findViewById<TextView>(R.id.tvuserEmail).text = Prefs.with(this@MainActivity).getString(AppConstants.DRIVER_EMAIL,"")
        binding.navView.setNavigationItemSelectedListener(this)

    }
    fun setData(){
        binding.toolbar.title.text = getString(R.string.home)
        binding.tvAppVersion.text = "Version: "+GeneralMethods.getAppVersion(this@MainActivity)

    }

    override fun onBackPressed() {
        if(binding.drawerLayout.isDrawerOpen(GravityCompat.START)){
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }else{
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                super.onBackPressed()
                finishAffinity()
            } else {
                // Toast.makeText(baseContext, getString(R.string.label_back_press), Toast.LENGTH_SHORT).show();


                Toast.makeText(this,getString(R.string.press_back_again_to_exit),Toast.LENGTH_LONG).show()

            }
            mBackPressed = System.currentTimeMillis()
        }

    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
       item.setChecked(true)

        binding.drawerLayout.closeDrawers()

        val id: Int = item.getItemId()

        when (id) {
          R.id.tripsFragment-> {
              binding.toolbar.title.text = getString(R.string.home)
               navController.navigate(R.id.tripsFragment)
            //  Toast.makeText(this@MainActivity,"trips",Toast.LENGTH_LONG).show()
          }
            R.id.changePasswordFragment-> {
                navController.navigate(R.id.changePasswordFragment)
                binding.toolbar.title.text = getString(R.string.change_password)

               // Toast.makeText(this@MainActivity,"ch",Toast.LENGTH_LONG).show()
            }
            R.id.signOut-> {
              //  Toast.makeText(this@MainActivity,"signout",Toast.LENGTH_LONG).show()
                GeneralMethods.signOutUser(this@MainActivity,getString(R.string.confirm_sign_out))

            }
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }


}