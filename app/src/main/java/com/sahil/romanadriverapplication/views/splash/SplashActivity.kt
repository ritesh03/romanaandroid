package com.sahil.romanadriverapplication.views.splash

import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.sahil.romanadriverapplication.R
import com.sahil.romanadriverapplication.databinding.ActivitySplashBinding
import com.sahil.romanadriverapplication.utils.AppConstants
import com.sahil.romanadriverapplication.utils.Prefs
import com.sahil.romanadriverapplication.views.LogInActivity
import com.sahil.romanadriverapplication.views.MainActivity
import com.sahil.romanadriverapplication.webservices.retrofit.RetofitClient

class SplashActivity : AppCompatActivity() {
    private val TAG = "SplashActivity"
    lateinit var binding:ActivitySplashBinding
    private var handler = Handler(Looper.myLooper()!!)
    private lateinit var prefs: Prefs
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
        handleTransition()

        enableNightmode()


    }

   fun init() {
       binding = DataBindingUtil.setContentView(this@SplashActivity,R.layout.activity_splash)
       RetofitClient.setUpRetrofitClient(this@SplashActivity)
       prefs = Prefs.with(this@SplashActivity)

   }
    private fun handleTransition(){
        handler.postDelayed({
            var intent = Intent()
            if(prefs.getString(AppConstants.ACCESS_TOKEN,"") == ""){
            intent =    Intent(this@SplashActivity, LogInActivity::class.java)
            }else{
              intent =  Intent(this@SplashActivity, MainActivity::class.java)
            }

            Log.d(TAG, "onCreate: assesstoken${prefs.getString(AppConstants.ACCESS_TOKEN,"")}")

            startActivity(intent)
            finishAffinity()

        }, 3000)
    }

    private fun enableNightmode() {
        val mode = this?.resources?.configuration?.uiMode?.and(Configuration.UI_MODE_NIGHT_MASK)
        when (mode) {
            Configuration.UI_MODE_NIGHT_YES -> {

              //  binding.activitySplash.setBackgroundColor(Color.parseColor("#000000"))
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.black)
                window.navigationBarColor= ContextCompat.getColor(this, R.color.black)
            }
            Configuration.UI_MODE_NIGHT_NO -> {
//                binding.activitySplash.setBackgroundColor(Color.WHITE)
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
                window.statusBarColor = ContextCompat.getColor(this, R.color.white)
                  window.navigationBarColor=ContextCompat.getColor(this, R.color.white)
            }
            Configuration.UI_MODE_NIGHT_UNDEFINED -> {}
        }
    }




}