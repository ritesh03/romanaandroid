package com.sahil.romanadriverapplication.views.trips

import android.Manifest
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.sahil.romanadriverapplication.R
import com.sahil.romanadriverapplication.adapters.AdapterTripList
import com.sahil.romanadriverapplication.databinding.FragmentTripsBinding
import java.util.*
import com.sahil.romanadriverapplication.utils.AppConstants
import com.sahil.romanadriverapplication.utils.GeneralMethods
import com.sahil.romanadriverapplication.utils.Prefs
import com.sahil.romanadriverapplication.views.pickup.PickUpActivity
import com.sahil.romanadriverapplication.webservices.response.createLocationResponse.CreateLocationApiResponse
import com.sahil.romanadriverapplication.webservices.response.tripsApiResponse.TripData
import com.sahil.romanadriverapplication.webservices.response.tripsApiResponse.TripsApiResponse
import repository.GPSTracker
import java.io.IOException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.Month
import java.time.Year
import java.time.LocalDate

import java.time.format.DateTimeFormatter
import java.time.format.TextStyle


class TripsFragment : Fragment(),View.OnClickListener,TripsContract.View,AdapterTripList.TripClick,DatePickerDialog.OnDateSetListener{
    lateinit var binding: FragmentTripsBinding
    private val TAG = "MainFragment"
    lateinit var presenter: TripsContract.Presenter
    lateinit var prefs: Prefs
    lateinit var adapterTripList: AdapterTripList
    var weekday = ""
    val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val printDateFormat = SimpleDateFormat("dd/MM/yyyy",Locale.getDefault())
    var printFormattedDate:String = ""
    var formattedDate: String = ""
    val calendar: Calendar = Calendar.getInstance()
    private var mDay:Int = 0
    private var mYear:Int = 0
    private var mMonth:Int = 0
    private var latitude:Double = 0.0
    private val REQUEST_ID_MULTIPLE_PERMISSIONS = 1001
    private var longitude:Double = 0.0
    private var selectedDay:Int=0
    private var selectedYear:Int=0
    private var selectedMonth:Int=0
    lateinit var calender:Calendar
    var dayOfWeekName = arrayOf(
       "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    )
    override fun onAttach(context: Context) {
        super.onAttach(context)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {}
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
      binding = FragmentTripsBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setData()
        setAdapeter()
        clickListner()
    }
    fun init(){
        prefs = Prefs.with(context)
        presenter = TripsPresenter()
        presenter.attachView(this)
        calender = Calendar.getInstance(TimeZone.getDefault())
        adapterTripList = AdapterTripList(context)
        checkAndRequestPermissions()

    }
    fun callCreareLocationApi(latitude:Double,longitude:Double){
        Log.d(TAG, "callCreareLocationApi: latitude:-- $latitude ,$longitude")
        presenter.performCreateLocation(prefs.getString(AppConstants.DRIVER_ID,""),latitude,longitude,AppConstants.ADMIN_ACCESS_TOKEN)
    }

    fun setAdapeter(){
        binding.rvAdapterMain.adapter = adapterTripList
    }
    fun setData(){
        Log.d(TAG, "setData calendr day: ${dayOfWeekName[calender.get(Calendar.DAY_OF_WEEK)-1]}")
        currentDayApiCall()
    }
    fun clickListner(){
         binding.ivRefresh.setOnClickListener(this@TripsFragment)
        binding.ivCalender.setOnClickListener(this@TripsFragment)
        binding.tvday.setOnClickListener(this@TripsFragment)
        adapterTripList.setTripOnClickListner(this)
    }
    companion object {
        @JvmStatic
        fun newInstance() =
            TripsFragment().apply {
                arguments = Bundle().apply {}
            }
    }
    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.tvday->{
                Log.d(TAG, "onClick: tvday")
                Log.d(TAG, "onClick: selectedday $selectedDay")
                if(selectedDay != 0){
                    mDay =   selectedDay
                    mMonth = selectedMonth
                    mYear =  selectedYear
                }else{
                    mDay =   calendar.get(Calendar.DAY_OF_MONTH)
                    mMonth = calendar.get(Calendar.MONTH)
                    mYear =  calendar.get(Calendar.YEAR)
                }
                val datePickerDialog =
                    DatePickerDialog(requireContext(), this, mYear,mMonth,mDay)
                datePickerDialog.show()
            }
            R.id.ivCalender->{
               Log.d(TAG, "onClick:  R.id.ivCalender")
                Log.d(TAG, "onClick: selectedday $selectedDay")
                if(selectedDay != 0){
                    mDay =   selectedDay
                    mMonth = selectedMonth
                    mYear =  selectedYear
                }else{
                mDay =   calendar.get(Calendar.DAY_OF_MONTH)
                mMonth = calendar.get(Calendar.MONTH)
                mYear =  calendar.get(Calendar.YEAR)}
                val datePickerDialog =
                    DatePickerDialog(requireContext(), this, mYear,mMonth,mDay)
                datePickerDialog.show()
           }
            R.id.ivRefresh->{
                Log.d(TAG, "setData: date  ${formattedDate}")
                presenter.performTripsApiCall(prefs.getString(AppConstants.DRIVER_ID, ""), formattedDate, "bearer" + prefs.getString(AppConstants.ACCESS_TOKEN, ""))
            }
        }
    }

    fun currentDayApiCall(){
        if(GeneralMethods.isNetworkActive(requireContext())) {
            selectedDay = 0
            selectedMonth =0
            selectedYear =0
            weekday = dayOfWeekName[calender.get(Calendar.DAY_OF_WEEK) - 1]
            formattedDate = df.format(calender.time)
            printFormattedDate = printDateFormat.format(calender.time)
            Log.d(TAG, "setData: date  ${formattedDate}")
            binding.tvday.text = printFormattedDate +" "+ weekday
            Log.d(TAG, "onClick:  R.id.ivRefresh")
            presenter.performTripsApiCall(prefs.getString(AppConstants.DRIVER_ID, ""), formattedDate, "bearer" + prefs.getString(AppConstants.ACCESS_TOKEN, "")
            )
        }
        else{
            GeneralMethods.showSnackBar(requireContext(),binding.parent, "No Internet")
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        checkAndRequestPermissions()
        getCurrentlocation()
        super.onResume()
        if(prefs.getString(AppConstants.NAVIGATED_TRIP_DATE,"") == ""){
            Log.d(TAG, "setData calendr day: ${dayOfWeekName[calender.get(Calendar.DAY_OF_WEEK)-1]}")
            currentDayApiCall()
        }
        else{
            formattedDate = prefs.getString(AppConstants.NAVIGATED_TRIP_DATE,"").toString().trim()
            printFormattedDate = prefs.getString(AppConstants.PRINT_TRIP_DATE,"")
            Log.d(TAG, "onResume: dates :--  $formattedDate  \n year :--  ${formattedDate.substring(0,4)}" +
                    "\n month :-- ${formattedDate.substring(5,7)}" +
                    "\n day :-- ${formattedDate.substring(8)}")
            selectedYear = Integer.parseInt(formattedDate.substring(0,4))
            try{

                selectedMonth = Integer.parseInt(formattedDate.substring(5,7))
                selectedDay = Integer.parseInt(formattedDate.substring(8) )
            }catch (e:Exception){
                Log.d(TAG, "onResume: ${e.message.toString()}")
                selectedMonth = Integer.parseInt(formattedDate.substring(5,6))
                selectedDay = Integer.parseInt(formattedDate.substring(7) )
            }

            binding.tvday.text = printFormattedDate
            presenter.performTripsApiCall(prefs.getString(AppConstants.DRIVER_ID, ""), formattedDate, "bearer" + prefs.getString(AppConstants.ACCESS_TOKEN, ""))
            prefs.remove(AppConstants.NAVIGATED_TRIP_DATE)
            prefs.remove(AppConstants.PRINT_TRIP_DATE)
        }
    }

    override fun apiSuccess(data: TripsApiResponse) {
        Log.d(TAG, "apiSuccess: ${Gson().toJson(data)}")

        if(data.data.tripData.size == 0){
            binding.tvNoTripsFound.visibility = View.VISIBLE
            binding.rvAdapterMain.visibility = View.GONE
            adapterTripList.notifyDataSetChanged()
        }else {
            binding.rvAdapterMain.visibility = View.VISIBLE
            binding.tvNoTripsFound.visibility = View.GONE
            adapterTripList.setTripData(data.data.tripData)
            adapterTripList.notifyDataSetChanged()
        }
    }


    override fun tripsFailure(failureMessage: String) {
        GeneralMethods.apiErrorAlertDialog(requireContext(),getString(R.string.connection_error))
    }

    override fun tripsError(errorMessage: String) {
        GeneralMethods.apiErrorAlertDialog(requireContext(),errorMessage)

    }

    override fun showLoading() {
        binding.progressBar.visibility = View.VISIBLE

    }

    override fun dismissLoading() {
        binding.progressBar.visibility = View.GONE

    }

    override fun apicreateLocationApiSuccess(data: CreateLocationApiResponse) {
        Log.d(TAG, "apicreateLocationApiSuccess: ${Gson().toJson(data)}")
    }
    override fun apiCreteLocationError(error: String) {

        Log.d(TAG, "apiCreteLocationError: $error")
        GeneralMethods.apiErrorAlertDialog(requireContext(),error)
    }

    override fun apiCreateLocationFaliure(faliure: String) {
        GeneralMethods.apiErrorAlertDialog(requireContext(),getString(R.string.connection_error))
    }

    override fun sessionExpired() {
        GeneralMethods.tokenExpired(requireContext(),getString(R.string.session_expire))
    }
    override fun onTripClick(data: TripData) {
        Log.d(TAG, "onTripClick: Trip data ${Gson().toJson(data)}")
        Prefs.with(context).save(AppConstants.SELETED_TRIP_DATA,data)
        prefs.save(AppConstants.NAVIGATED_TRIP_DATE,formattedDate)
        prefs.save(AppConstants.PRINT_TRIP_DATE,binding.tvday.text.toString())
        val intent = Intent(context,PickUpActivity::class.java)
        startActivity(intent)

    }


    private fun checkAndRequestPermissions(): Boolean {
        val accessFineLocation = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
        val accessCourseLocation = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
    val listPermissionsNeeded = ArrayList<String>()

        if (accessFineLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION)
        }
        if (accessCourseLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(requireActivity(), listPermissionsNeeded.toTypedArray(), REQUEST_ID_MULTIPLE_PERMISSIONS)
            return false
        } else {
            return true
        }
    }
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        Log.d(TAG, "onDateSet: selected date = ${year}-${month}-${dayOfMonth}")
        formattedDate = "${year}-${month+1}-${dayOfMonth}"
        printFormattedDate = "${dayOfMonth}/${month+1}/${year}"
        selectedYear = year
        selectedMonth = month
        selectedDay = dayOfMonth
        val simpledateformat = SimpleDateFormat("EEEE")
        val date = Date(year, month, dayOfMonth - 1)
        val dayOfWeek = simpledateformat.format(date)
        weekday = dayOfWeek
        Log.d(TAG, "onDateSet: $dayOfWeek}")
        binding.tvday.text = printFormattedDate +" "+dayOfWeek
        presenter.performTripsApiCall(prefs.getString(AppConstants.DRIVER_ID,""),formattedDate,prefs.getString(AppConstants.ACCESS_TOKEN,""))
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when(requestCode){

            REQUEST_ID_MULTIPLE_PERMISSIONS->{

                val perms = HashMap<String,Int>()

                perms[Manifest.permission.ACCESS_FINE_LOCATION] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.ACCESS_COARSE_LOCATION] = PackageManager.PERMISSION_GRANTED

                if(grantResults.size > 0){
                    for(i in permissions.indices) perms[permissions[i]] = grantResults[i]

                    //check for all permissions
                    if(perms[Manifest.permission.ACCESS_COARSE_LOCATION] == PackageManager.PERMISSION_GRANTED &&
                        perms[Manifest.permission.ACCESS_FINE_LOCATION] == PackageManager.PERMISSION_GRANTED
                    ){
                        if(checkAndRequestPermissions() == true){
                            getCurrentlocation()
                        }
                    }
                    else {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)) {
                            showDialogOK("Service Permissions are required for this app",
                                DialogInterface.OnClickListener { dialog, which ->
                                    when (which) {
                                        DialogInterface.BUTTON_POSITIVE -> checkAndRequestPermissions()
                                        DialogInterface.BUTTON_NEGATIVE ->   explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?")

                                    }
                                })
                        } else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?")
                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }

                }


            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getCurrentlocation() {
       val gps = GPSTracker(requireContext())
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude()
            longitude = gps.getLongitude()

            Log.e(TAG, "$latitude  $longitude")
            val geocoder: Geocoder
            val returnAddress: Address
            val addresses: List<Address>
            geocoder = Geocoder(requireContext(), Locale.getDefault())
            try {
                if (latitude != 0.0 && longitude != 0.0) {
                    addresses = geocoder.getFromLocation(
                        latitude,
                        longitude,
                        2
                    ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    returnAddress = addresses[0]
                    Log.e("returnAddress", addresses.toString())

                 callCreareLocationApi(latitude,longitude)
                }

            } catch (e: IOException) {
                Log.e("error message", e.message.toString())
                e.printStackTrace()
            }
        } else {
            gps.showSettingsAlert()
        }
    }
    private fun explain(msg: String) {
        val dialog = androidx.appcompat.app.AlertDialog.Builder(requireContext())
        dialog.setMessage(msg)
            .setPositiveButton("Yes") { paramDialogInterface, paramInt ->
                //  permissionsclass.requestPermission(type,code);
                startActivity(
                    Intent(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:com.codebrew.whrzat")
                    )
                )
            }
            .setNegativeButton("Cancel") { paramDialogInterface, paramInt ->
                // Toast.makeText(this@HomeActivity, "Required", Toast.LENGTH_LONG).show()
            }
        dialog.show()
    }

    private fun showDialogOK(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(requireContext())
            .setMessage(message)
            .setPositiveButton("OK", okListener)
            .setNegativeButton("Cancel", okListener)
            .create()
            .show()
    }

}