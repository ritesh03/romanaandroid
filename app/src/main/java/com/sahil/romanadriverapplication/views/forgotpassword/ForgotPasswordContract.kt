package com.sahil.romanadriverapplication.views.forgotpassword

import com.sahil.romanadriverapplication.views.login.LogInContract
import com.sahil.romanadriverapplication.webservices.response.loginApiResponse.LoginResponse

class ForgotPasswordContract {


interface View {

    fun forgotPasswordSuccess(success:String)


    fun forgotPasswordError(errorMessage: String)

    fun forgotPasswordFailure(failureMessage: String)

    fun showLoading()

    fun dismissLoading()

    fun sessionExpired()
   }

    interface Presenter {
        fun performForgotPassword(email: String)

        fun attachView(view: ForgotPasswordContract.View)

        fun detachView()

    }

}