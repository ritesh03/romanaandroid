package com.sahil.romanadriverapplication.views.changePassword

import android.util.Log
import com.sahil.romanadriverapplication.utils.AppConstants
import com.sahil.romanadriverapplication.webservices.response.updatePasswordResponse.UpdatePasswordResponse
import com.sahil.romanadriverapplication.webservices.retrofit.RetofitClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePasswordPresenter:ChangePasswordContract.Presenter {

   private var view:ChangePasswordContract.View ?= null
    private  val TAG = "ChangePasswordPresenter"
    override fun performChangePassword(oldPassword:String,password: String, driverId: String, accessToken: String) {
        view!!.showLoading()
        val map = HashMap<String,String>()
        map.put("uniquieAppKey",AppConstants.APP_UNIQUE_KEY)
        map.put("id",driverId)
        map.put("password",password)
        map.put("oldPassword",oldPassword)
        RetofitClient.get().apiUpdatePassword(map).enqueue(object: Callback<UpdatePasswordResponse>{
            override fun onResponse(call: Call<UpdatePasswordResponse>, response: Response<UpdatePasswordResponse>) {
                Log.d(TAG, "onResponse: ${response.code()}")
                if(view!=null){
                    if(response.isSuccessful){
                        response.body()?.let { view!!.apiSuccess(it) }
                        view!!.dismissLoading()
                    }else if(response.code() == AppConstants.UnAuthorized){
                        view!!.sessionExpire()
                        view!!.dismissLoading()
                    }
                    else{
                       view!!.apiError(JSONObject(response.errorBody()!!.string()).getString("message"))
                        view!!.dismissLoading()
                    }
                }
            }

            override fun onFailure(call: Call<UpdatePasswordResponse>, t: Throwable) {
                view!!.dismissLoading()
                Log.d(TAG, "onFailure: ${t.message.toString()}")
                    view!!.apiFaliure(t.message.toString())


            }
        })
    }

    override fun attachView(view: ChangePasswordContract.View) {
    this.view = view
    }

    override fun detachView() {}
}