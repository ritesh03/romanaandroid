package com.sahil.romanadriverapplication.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.sahil.romanadriverapplication.R
import com.sahil.romanadriverapplication.databinding.ActivityLogInBinding

class LogInActivity : AppCompatActivity() {
    lateinit var binding:ActivityLogInBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

   fun init(){
       binding = DataBindingUtil.setContentView(this@LogInActivity,R.layout.activity_log_in)
   }
}