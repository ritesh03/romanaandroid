package com.sahil.romanadriverapplication.views.trips

import com.sahil.romanadriverapplication.webservices.response.createLocationResponse.CreateLocationApiResponse
import com.sahil.romanadriverapplication.webservices.response.tripsApiResponse.TripsApiResponse

class TripsContract {

    interface View {
        fun apiSuccess(data: TripsApiResponse)
        fun tripsFailure( failureMessage:String)
        fun tripsError( errorMessage : String)
        fun showLoading()
        fun dismissLoading()
        fun apicreateLocationApiSuccess(data: CreateLocationApiResponse)
        fun apiCreteLocationError(error: String)
        fun apiCreateLocationFaliure(faliure: String)
        fun sessionExpired()
    }
    interface Presenter {
        fun performTripsApiCall(driverId:String,date:String,accessToken:String)
        fun attachView(view: TripsContract.View)
        fun performCreateLocation(driverId:String,latitude:Double,longitude:Double,accessToken: String)
        fun detachView()

    }
}