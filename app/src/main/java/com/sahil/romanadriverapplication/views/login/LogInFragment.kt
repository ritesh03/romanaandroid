package com.sahil.romanadriverapplication.views.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.google.gson.Gson
import com.sahil.romanadriverapplication.R
import com.sahil.romanadriverapplication.databinding.FragmentLogInBinding
import com.sahil.romanadriverapplication.utils.AppConstants
import com.sahil.romanadriverapplication.utils.GeneralMethods
import com.sahil.romanadriverapplication.utils.Prefs
import com.sahil.romanadriverapplication.views.MainActivity
import com.sahil.romanadriverapplication.webservices.response.loginApiResponse.LoginResponse

class LogInFragment : Fragment(),View.OnClickListener,LogInContract.View {
    private  val TAG = "LogInFragment"
    lateinit var binding:FragmentLogInBinding
    lateinit var presenter: LogInContract.Presenter
    lateinit var prefs:Prefs


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {}
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLogInBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        clickListners()

    }
    fun init(){
        prefs = Prefs.with(context)
        presenter = LogInPresenter()
        presenter.attachView(this)
        binding.toolbar.title.text = getString(R.string.signin)
//        binding.etEmail.setText("driver@gmail.com")
//        binding.etPassword.setText("driver1234")
    }

    fun clickListners(){
        binding.btnLogIn.setOnClickListener(this)
        binding.tvForgotPassword.setOnClickListener(this)
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            LogInFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun onClick(v: View?) {
        when(v!!.id){

         R.id.btnLogIn->{
             if(GeneralMethods.isNetworkActive(requireContext())) {
               if(checkvalidation()) {
                   presenter.performLogin(
                       binding.etEmail.text.toString(),
                       binding.etPassword.text.toString()
                   )
               }

             }else{
                 GeneralMethods.showSnackBar(requireContext(),binding.parent,getString(R.string.connection_error))
             }
          }
            R.id.tvForgotPassword->{
                binding.tvForgotPassword.findNavController().navigate(R.id.action_logInFragment_to_forgotPasswordFragment)
            }
        }
    }

    fun checkvalidation():Boolean{
         if(binding.etEmail.text.toString().trim().isEmpty()){
             GeneralMethods.showSnackBar(requireContext(),binding.parent,getString(R.string.enter_email))
             return false
         }
        else if(binding.etPassword.text.toString().trim().isEmpty()){
             GeneralMethods.showSnackBar(requireContext(),binding.parent,getString(R.string.enter_password))
             return false
         }
        else{
            return true
         }
    }





    override fun loginSuccess(data: LoginResponse) {
        prefs.save(AppConstants.ACCESS_TOKEN,"bearer "+data.data.access_token.toString())
        prefs.save(AppConstants.DRIVER_ID,data.data.data.id)
        prefs.save(AppConstants.USER_NAME,data.data.data.Name+" "+data.data.data.lastName)
        prefs.save(AppConstants.DRIVER_EMAIL,data.data.data.email)
        Log.d(TAG, "loginSuccess: ${Gson().toJson(data)} ")
        val intent = Intent(context,MainActivity::class.java)
        startActivity(intent)
    }


    override fun loginError(errorMessage: String) {
        GeneralMethods.apiErrorAlertDialog(requireContext(),errorMessage)
    }


    override fun loginFailure(failureMessage: String) {
        GeneralMethods.apiErrorAlertDialog(requireContext(),getString(R.string.connection_error))
    }



    override fun showLoading() {
        binding.progressBar.visibility = View.VISIBLE
    }

    override fun dismissLoading() {
        binding.progressBar.visibility = View.GONE
    }

    override fun sessionExpired() {
        GeneralMethods.tokenExpired(requireContext(),getString(R.string.session_expire))
    }
}