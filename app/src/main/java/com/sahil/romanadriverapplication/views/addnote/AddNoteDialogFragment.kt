package com.sahil.romanadriverapplication.views.addnote

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.gson.Gson
import com.sahil.romanadriverapplication.R
import com.sahil.romanadriverapplication.databinding.DialogFragmentAddNoteBinding
import com.sahil.romanadriverapplication.utils.AppConstants
import com.sahil.romanadriverapplication.utils.GeneralMethods
import com.sahil.romanadriverapplication.utils.Prefs
import com.sahil.romanadriverapplication.views.tripDetail.TripDetailFragment
import com.sahil.romanadriverapplication.views.tripDetail.TripDetailFragmentDirections
import com.sahil.romanadriverapplication.webservices.response.addnotesresponse.AddNotesResponse

class AddNoteDialogFragment : DialogFragment(),View.OnClickListener {
    lateinit var binding:DialogFragmentAddNoteBinding
    private  val TAG = "AddNoteDialogFragment"
    lateinit var prefs: Prefs
   //lateinit var apiNoteAdded:ApiNoteAdded
    val args: AddNoteDialogFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
       binding = DialogFragmentAddNoteBinding.inflate(inflater,container,false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setData()
        clickListners()
    }
    fun init(){
        prefs = Prefs.with(context)
       // apiNoteAdded = ApiNoteAdded()

    }
    fun setData(){
        binding.etAddNote.setText(args.tripId.toString())
    }
    fun clickListners(){
        binding.tvSave.setOnClickListener(this)
        binding.tvCancel.setOnClickListener(this)
    }
    companion object {
        @JvmStatic
        fun newInstance() =
            AddNoteDialogFragment().apply {
                arguments = Bundle().apply {}
            }
    }

    override fun onClick(v: View?) {
      when(v!!.id){
          R.id.tvSave->{
              if(checkValidation()){
                  Log.d(TAG, "onClick: acesstoken :---- ${prefs.getString(AppConstants.ACCESS_TOKEN,"")}")
//                presenter.performCreateNotes(prefs.getString(AppConstants.DRIVER_ID,""),
//                    args.tripId,
//                    binding.etAddNote.text.toString(),
//                    prefs.getString(AppConstants.ACCESS_TOKEN,""))
                  val liveData:LiveData<String> = goNext(binding.etAddNote.text.toString())
                  val navController = findNavController()
                  navController.previousBackStackEntry?.savedStateHandle?.set("addNoteRes", liveData.value)
                  dismiss()
              }
          }
          R.id.tvCancel->{
              dismiss()
          }
      }
    }

    private fun checkValidation():Boolean{
        if(binding.etAddNote.text.toString().isNullOrEmpty()){
            GeneralMethods.showSnackBar(requireContext(),binding.parent,getString(R.string.please_enter_note))
            return false
        }
        else{
            return true
        }
    }


     fun goNext(data:String):MutableLiveData<String>{
        val liveData:MutableLiveData<String> = MutableLiveData()
        liveData.value =data
       return liveData
    }

}
