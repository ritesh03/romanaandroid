package com.sahil.romanadriverapplication.views.login

import android.util.Log
import com.google.gson.Gson
import com.sahil.romanadriverapplication.utils.AppConstants
import com.sahil.romanadriverapplication.webservices.response.loginApiResponse.LoginResponse
import com.sahil.romanadriverapplication.webservices.retrofit.RetofitClient
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class LogInPresenter :LogInContract.Presenter {
    private  val TAG = "LogInPresenter"

    private var view: LogInContract.View? = null
    override fun performLogin(email: String, password: String) {
        view!!.showLoading()
        val map = HashMap<String,String>()
        map.put("email",email)
        map.put("password",password)
        map.put("uniquieAppKey",AppConstants.APP_UNIQUE_KEY)
        Log.d(TAG, "performLogin: Parms:-- ${Gson().toJson(map)}")
        RetofitClient.get().postApiLogIn(map).enqueue(object :Callback<LoginResponse>{
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                Log.d(TAG, "onResponse: ${response.code()}")
                if (response.isSuccessful && response.code() == 200) {
                    response.body()?.let { view?.loginSuccess(it) }
                    view!!.dismissLoading()

                }
                else if (response.code() == AppConstants.UnAuthorized){
                    view!!.sessionExpired()
                }
                else {
                    Log.d(TAG, "onResponse: ${response.errorBody()}")
                        try {
                            view?.loginError(JSONObject(response.errorBody()!!.string()).getString("message"))
                            view!!.dismissLoading()
                        } catch (e: JSONException) {
                            e.printStackTrace()
                            view!!.dismissLoading()
                        } catch (e: IOException) {
                            e.printStackTrace()
                            view!!.dismissLoading()
                        }

                }
            }
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                Log.d(TAG, "onFailure: ${t.message}")
                if(view != null){
                    view?.loginFailure(t.message.toString());
                    view!!.dismissLoading()
                }
            }

        })
    }

    override fun attachView(view: LogInContract.View) {
       this.view = view
    }

    override fun detachView() {}


}