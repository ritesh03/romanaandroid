package com.sahil.romanadriverapplication.views.tripDetail

import com.sahil.romanadriverapplication.webservices.response.addnotesresponse.AddNotesResponse
import com.sahil.romanadriverapplication.webservices.response.createLocationResponse.CreateLocationApiResponse
import com.sahil.romanadriverapplication.webservices.response.tripupadteresponse.TripUpdateApiResponse

class TripDetailContract {

    interface View{
        fun apiSuccess(data: AddNotesResponse)
        fun apiError(error:String)
        fun apiFaliure(faliure:String)
        fun apiTripUpdateSuccess(data: TripUpdateApiResponse)
        fun apiTripUpdateError(error:String)
        fun apitripUpdateFaliure(faliure:String)
        fun sessionExpire()
        fun showLoading()
        fun dismissLoading()
    }
    interface Presenter {
        fun performCreateNotes(driverId:String,tripId:String,note:String,accessToken:String)
        fun performTripUpadte(driverId:String,tripId:String,tripStatus:Int,accessToken: String)
        fun attachView(view: TripDetailContract.View)
        fun detachView()

    }
}
