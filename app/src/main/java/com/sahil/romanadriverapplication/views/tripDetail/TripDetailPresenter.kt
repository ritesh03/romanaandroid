package com.sahil.romanadriverapplication.views.tripDetail

import android.util.Log
import com.google.gson.Gson
import com.sahil.romanadriverapplication.utils.AppConstants
import com.sahil.romanadriverapplication.webservices.response.addnotesresponse.AddNotesResponse
import com.sahil.romanadriverapplication.webservices.response.tripupadteresponse.TripUpdateApiResponse
import com.sahil.romanadriverapplication.webservices.retrofit.RetofitClient
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException


class TripDetailPresenter:TripDetailContract.Presenter {
    private val TAG = "TripDetailPresenter"

    lateinit var view: TripDetailContract.View

    override fun performCreateNotes(driverId: String, tripId: String, note: String, accessToken: String) {

        val map:HashMap<String,String> = HashMap()
        map.put("uniquieAppKey", AppConstants.APP_UNIQUE_KEY)
        map.put("driverId",driverId)
        map.put("tripId",tripId)
        map.put("note",note)
        Log.d(TAG, "performCreateNotes: params:---  ${Gson().toJson(map)}")
        RetofitClient.get().apiCreteNote(accessToken,map).enqueue(object :
            Callback<AddNotesResponse> {
            override fun onResponse(
                call: Call<AddNotesResponse>,
                response: Response<AddNotesResponse>
            ) {
                if(response.isSuccessful){
                    if(view!= null) {
                        Log.d(TAG, "onResponse: ${Gson().toJson(response.body())}")
                        response.body()?.let { view.apiSuccess(it) }
                    }
                }
                else if(response.code() == AppConstants.UnAuthorized){
                    view.sessionExpire()
                }
                else{
                    try {
                        view.apiError(JSONObject(response.errorBody()!!.string()).getString("message"))
                        view.dismissLoading()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        view.dismissLoading()
                    } catch (e: IOException) {
                        e.printStackTrace()
                        view.dismissLoading()
                    }
                }
            }

            override fun onFailure(call: Call<AddNotesResponse>, t: Throwable) {
                Log.d(TAG, "onFailure: ${t.message}")
                view.apiFaliure(t.message.toString())
            }

        })



    }

    override fun performTripUpadte(driverId: String, tripId: String, tripStatus: Int, accessToken: String) {
        view.showLoading()
        val map:HashMap<String,Any> = HashMap()
        map.put("id",tripId)
        map.put("driverName",driverId)
        map.put("tripStatus",tripStatus)
        map.put("uniquieAppKey",AppConstants.APP_UNIQUE_KEY)
        RetofitClient.get().apiTripUpdate(accessToken,map).enqueue(object :Callback<TripUpdateApiResponse>{
            override fun onResponse(call: Call<TripUpdateApiResponse>, response: Response<TripUpdateApiResponse>) {

                if(response.isSuccessful){
                    response.body()?.let { view?.apiTripUpdateSuccess(it) }
                    view.dismissLoading()
                }
                else if ( response.code() == AppConstants.UnAuthorized){
                    view.sessionExpire()
                    view.dismissLoading()
                }else{
                    try {
                        view.apiTripUpdateError(JSONObject(response.errorBody()!!.string()).getString("message"))
                        view.dismissLoading()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        view.dismissLoading()
                    } catch (e: IOException) {
                        e.printStackTrace()
                        view.dismissLoading()
                    }
                    view.dismissLoading()
                }
            }

            override fun onFailure(call: Call<TripUpdateApiResponse>, t: Throwable) {
                Log.d(TAG, "onFailure: tripUpdate :-  ${t.message}")
                view.apitripUpdateFaliure(t.message.toString())
                view.dismissLoading()
            }

        })
    }


    override fun attachView(view: TripDetailContract.View) {
        this.view = view
    }

    override fun detachView() {
    }
}