package com.sahil.romanadriverapplication.views.changePassword

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import com.sahil.romanadriverapplication.R
import com.sahil.romanadriverapplication.databinding.FragmentChangePasswordBinding
import com.sahil.romanadriverapplication.databinding.FragmentLogInBinding
import com.sahil.romanadriverapplication.utils.AppConstants
import com.sahil.romanadriverapplication.utils.GeneralMethods
import com.sahil.romanadriverapplication.utils.Prefs
import com.sahil.romanadriverapplication.webservices.response.updatePasswordResponse.UpdatePasswordResponse

class ChangePasswordFragment : Fragment(),View.OnClickListener,ChangePasswordContract.View {

    private val TAG = "ChangePasswordFragment"
    lateinit var binding:FragmentChangePasswordBinding
    lateinit var presenter: ChangePasswordContract.Presenter
    lateinit var prefs: Prefs

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentChangePasswordBinding.inflate(inflater,container,false)
        return binding.root
        }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setData()
        setClickListners()
    }
    fun init(){
        prefs = Prefs.with(context)
        presenter = ChangePasswordPresenter()
        presenter.attachView(this)
    }


  fun setData(){

    }
    fun setClickListners(){
     binding.tvUpdate.setOnClickListener(this)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            ChangePasswordFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.tvUpdate->{
                binding.etConfirmPassword.clearFocus()
                binding.etNewPassword.clearFocus()
                binding.etOldPassword.clearFocus()
             GeneralMethods.hideKeyboardFrom(requireContext(),binding.tvUpdate)
                if(checkValidation()) {
                    presenter.performChangePassword(
                        binding.etOldPassword.text.toString().trim(),
                        binding.etNewPassword.text.toString().trim(),
                        prefs.getString(AppConstants.DRIVER_ID, ""),
                        prefs.getString(AppConstants.ACCESS_TOKEN, "")
                    )
                }
            }
        }
    }

    fun checkValidation():Boolean{
        if(binding.etOldPassword.text.isNullOrEmpty()){
            GeneralMethods.showSnackBar(requireContext(),binding.parent,getString(R.string.please_enter_old_password))
            return false
        }
       else if(binding.etNewPassword.text.isNullOrEmpty()){
            GeneralMethods.showSnackBar(requireContext(),binding.parent,getString(R.string.please_enter_new_password))
            return false
        }
       else if(binding.etConfirmPassword.text.toString().trim().isNullOrEmpty()){
            GeneralMethods.showSnackBar(requireContext(),binding.parent,getString(R.string.please_enter_confirm_password))
            return false
        }
       else if(!(binding.etNewPassword.text.toString().trim().equals(binding.etConfirmPassword.text.toString().trim()))){
            GeneralMethods.showSnackBar(requireContext(),binding.parent,getString(R.string.new_and_confirm_password_not_match))
            return false
        }
        else{
            return true
        }
    }

    override fun apiSuccess(data: UpdatePasswordResponse) {
        Log.d(TAG, "apiSuccess: ${Gson().toJson(data)}")

        binding.etConfirmPassword.setText("")
        binding.etOldPassword.setText("")
        binding.etNewPassword.setText("")
        //Toast.makeText(context,"Password updated",Toast.LENGTH_LONG).show()
        GeneralMethods.apiErrorAlertDialog(requireContext(),getString(R.string.password_updated_successfully))
    }

    override fun apiError(error: String) {
        GeneralMethods.apiErrorAlertDialog(requireContext(),error)
    }

    override fun apiFaliure(faliure: String) {

        GeneralMethods.apiErrorAlertDialog(requireContext(),getString(R.string.connection_error))

    }

    override fun sessionExpire() {
     GeneralMethods.tokenExpired(requireContext(),getString(R.string.session_expire))
    }

    override fun showLoading() {
        binding.progressBar.visibility = View.VISIBLE
    }

    override fun dismissLoading() {
        binding.progressBar.visibility = View.GONE
    }
}