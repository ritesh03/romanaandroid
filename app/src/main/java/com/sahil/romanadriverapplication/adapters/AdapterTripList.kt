package com.sahil.romanadriverapplication.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sahil.romanadriverapplication.R
import com.sahil.romanadriverapplication.utils.AppConstants
import com.sahil.romanadriverapplication.webservices.response.tripsApiResponse.TripData

class AdapterTripList(var context: Context?) : RecyclerView.Adapter<AdapterTripList.ViewHolder>() {
    private  val TAG = "AdapterMain"
    lateinit var tripClick: TripClick
   var data:List<TripData> = ArrayList()
   inner class ViewHolder(itemView:View): RecyclerView.ViewHolder(itemView) {
       init {
           itemView.setOnClickListener {
               tripClick.onTripClick(data[adapterPosition])
           }
       }
        var tvStartTime = itemView.findViewById<TextView>(R.id.tvitemTittle)
        var tvDescription = itemView.findViewById<TextView>(R.id.tvitemDescription)

       fun bindItem(tripdata: TripData){
           Log.d(TAG, "bindItem: ${tripdata.tripTime +" Start of shift"}")
           Log.d(TAG, "bindItem: ${ "Driving in "+tripdata.DestinationAddress.streetAddress+", "+tripdata.DestinationAddress.city+ ", "+tripdata.DestinationAddress.country}")

           when(tripdata.tripRequest){
               AppConstants.PICKUP->{
                   tvStartTime.text =  "${tripdata.tripTime} ${context!!.getString(R.string.pick_up)} ${tripdata.passengerName.firstName}"
               }
               AppConstants.DROPOFF->{
                   tvStartTime.text =  "${tripdata.tripTime} ${context!!.getString(R.string.drop_off)} ${tripdata.passengerName.firstName}"
               }
               AppConstants.WILLCALL->{
                   tvStartTime.text =  "${tripdata.tripTime} ${context!!.getString(R.string.will_call)} ${tripdata.passengerName.firstName}"
               }
               }
           tvDescription.text = "Driving in "+tripdata.DestinationAddress.streetAddress+", "+tripdata.DestinationAddress.city+ ", "+tripdata.DestinationAddress.country
       }
    }

    fun setTripData(tripdata: List<TripData>){
        this.data = tripdata
    }
    fun setTripOnClickListner(tripClick: TripClick){
        Log.d(TAG, "setTripOnClickListner: ")
        this.tripClick = tripClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterTripList.ViewHolder {
       return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_trip_list,parent,false))
    }

    override fun onBindViewHolder(holder: AdapterTripList.ViewHolder, position: Int) {
       val sata = data[position]
       // holder.tvStartTime.text = sata.tripTime
        holder.bindItem(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }


    interface TripClick{
        fun onTripClick(data:TripData)
    }
}