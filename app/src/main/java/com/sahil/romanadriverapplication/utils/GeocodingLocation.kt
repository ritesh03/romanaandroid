package com.sahil.romanadriverapplication.utils

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


object GeocodingLocation {
    val bundle = Bundle()
        private const val TAG = "GeocodingLocation"
        fun getAddressFromLocation(locationAddress: String, context: Context?, handler: Handler?) {
            val thread: Thread = object : Thread() {
                override fun run() {
                    val geocoder = Geocoder(context, Locale.getDefault())
                    var result: String? = null
                    try {
                        val addressList: MutableList<Address>? = geocoder.getFromLocationName(locationAddress, 1)
                        if (addressList != null && addressList.size > 0) {
                            val address: Address = addressList[0]
                            val sb = StringBuilder()
                            sb.append(address.getLatitude()).append(",")
                            sb.append(address.getLongitude())
//                            bundle.putString("latitude",address.latitude.toString())
//                            bundle.putString("longitude",address.longitude.toString())
                            result = sb.toString()
                        }
                    } catch (e: IOException) {
                        Log.e(TAG, "Unable to connect to Geocoder", e)
                    } finally {
                        val message: Message = Message.obtain()
                        message.setTarget(handler)
                        if (result != null) {
                            message.what = 1

//                            result = "${result}".trimIndent()
                            bundle.putString("address", result)

                            message.setData(bundle)
                        } else {
                            message.what = 1
                            val bundle = Bundle()
                          //  result = """Address: $locationAddress Unable to get Latitude and Longitude for this address location."""
                            bundle.putString("address", result)
                            message.setData(bundle)
                        }
                        message.sendToTarget()
                    }
                }
            }
            thread.start()
        }
    }


//
//Intent mapIntent = new Intent(Intent.ACTION_VIEW,Uri.parse(address));
//
//mapIntent.setPackage("com.google.android.apps.maps");
//if (mapIntent.resolveActivity(getPackageManager()) != null) {
//    startActivity(mapIntent);
//}
//else {
//    Toast.makeText(NavActivity.this, "error", Toast.LENGTH_SHORT).show();
//
//}