package com.sahil.romanadriverapplication.utils


import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.icu.text.DateTimePatternGenerator.PatternInfo.OK
import android.net.ConnectivityManager
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.sahil.romanadriverapplication.R
import com.sahil.romanadriverapplication.views.LogInActivity
import android.app.Activity

import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.FragmentActivity

object GeneralMethods {

    fun showSnackBar(context: Context, parentView: View?, msg: String?) {
        val snackbar = Snackbar.make(parentView!!, msg!!, Snackbar.LENGTH_LONG)
        snackbar.setAction(
            context.getString(R.string.OK)
        ) { snackbar.dismiss() }.show()

        //set color of action button text
        snackbar.setActionTextColor(ContextCompat.getColor(context, R.color.skyBlue))

        //set color of snackbar text
        val view = snackbar.view.findViewById<TextView>(R.id.snackbar_text)
        view.setTextColor(ContextCompat.getColor(context, R.color.white))
    }

    fun isNetworkActive(context: Context?): Boolean {
        if (context != null) {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnectedOrConnecting
        }
        return false
    }
    fun getAppVersion(context: Context):String{
        var version=" ";
        try {
            val pInfo: PackageInfo =
                    context.getPackageManager().getPackageInfo(context.getPackageName(), 0)
            version = pInfo.versionName

        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        return version
    }

    fun tokenExpired(mContext: Context,message:String) {
        AlertDialog.Builder(mContext)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok) { dialogInterface, i ->
                Prefs.with(mContext).removeAll()
                val intent = Intent(mContext, LogInActivity::class.java)
                mContext.startActivity(intent)
                (mContext as AppCompatActivity).finishAffinity()
                mContext.overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right)
            }
            .setCancelable(false)
            .show()
    }
    fun signOutUser(mContext: Context,message:String) {
        AlertDialog.Builder(mContext)
            .setMessage(message)
            .setPositiveButton("Yes") { dialogInterface, i ->
                Prefs.with(mContext).removeAll()
                val intent = Intent(mContext, LogInActivity::class.java)
                mContext.startActivity(intent)
                (mContext as AppCompatActivity).finishAffinity()
                mContext.overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right)
            }
            .setNegativeButton("No"){ dialogInterface, i ->
                dialogInterface.dismiss()

            }

            .show()
    }
    fun apiErrorAlertDialog(mContext: Context,message:String) {
        AlertDialog.Builder(mContext)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok) { dialogInterface, i ->
                dialogInterface.dismiss()
            }
            .setCancelable(false)
            .show()
    }



    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
    fun hideKeyboardFrom(context: Context, view: View) {
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}
