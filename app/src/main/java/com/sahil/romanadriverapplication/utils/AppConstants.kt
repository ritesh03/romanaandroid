package com.sahil.romanadriverapplication.utils

object AppConstants {
    const val DAY_OF_WEEK = "dayofweek"
    const val DAY_OF_WEEK_INT_VALUE = "daybreakValue"
    const val BASE_URL = "http://52.8.254.207:8004/"
    const val APP_UNIQUE_KEY = "a873db9ec1764cfa32b66962e92bf5bd"
    const val UnAuthorized = 401
    const val DRIVER_ID = "driverId"
    const val DRIVER_EMAIL = "driverEmail"
    const val ACCESS_TOKEN = "accessToken"
    const val SELETED_TRIP_DATA = "selectedTripData"
    const val USER_NAME = "userName"
    const val PICKUP = 0
    const val DROPOFF = 1
    const val WILLCALL = 2
    const val ADMIN_ACCESS_TOKEN = "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxOTI0NTc5YTZlMjZlMzQ3NjE3Mzk1MyIsInVzZXJuYW1lIjoiUGFybWluZGVyIFNpbmdoIiwidHlwZSI6IkFETUlOIiwiaWF0IjoxNjM5MDI2NjEzfQ.9BMDH8Wu55ACSQWS7dw2aQlCNFF-5Yh5H4kICuYx3rA"
    const val NOTES_DATA = "notesdata"
    const val NAVIGATED_TRIP_DATE = "navigated_trip_date"
    const val PRINT_TRIP_DATE = "print_trip_date"
    const val UNSCHEDULED = 0
    const val SCHEDULED = 1
    const val DRIVER_ON_ROUTE = 2
    const val PICKEDUP = 3
    const val DELIVERED = 4
    const val CANCELED = 5
    const val NO_SHOW = 6
    const val MISSED = 7
    const val REROUTE = 8
}