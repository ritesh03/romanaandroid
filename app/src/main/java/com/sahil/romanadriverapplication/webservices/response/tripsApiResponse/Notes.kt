package com.sahil.romanadriverapplication.webservices.response.tripsApiResponse

data class Notes(
    val _id:String,
    val createdAt:String,
    val note:String
)
