package com.sahil.romanadriverapplication.webservices.response.addnotesresponse

data class Note(
    val _id: String,
    val createdAt: String,
    val note: String
)