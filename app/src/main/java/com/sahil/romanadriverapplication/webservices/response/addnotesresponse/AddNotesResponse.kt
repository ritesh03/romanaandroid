package com.sahil.romanadriverapplication.webservices.response.addnotesresponse

data class AddNotesResponse(
    val `data`: Data,
    val message: String,
    val statusCode: Int
)