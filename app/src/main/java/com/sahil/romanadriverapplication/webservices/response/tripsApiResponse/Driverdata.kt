package com.sahil.romanadriverapplication.webservices.response.tripsApiResponse

data class Driverdata(
    val _id: Id,
    val count: Int
)