package com.sahil.romanadriverapplication.webservices.response.tripsApiResponse

data class OriginAddress(
    val _id: String,
    val city: String,
    val comments: String,
    val country: String,
    val name: String,
    val phoneNo: String,
    val postalCode: String,
    val state: String,
    val streetAddress: String,
    val suite: String
)