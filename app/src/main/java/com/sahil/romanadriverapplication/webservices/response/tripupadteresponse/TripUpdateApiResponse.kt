package com.sahil.romanadriverapplication.webservices.response.tripupadteresponse

data class TripUpdateApiResponse(
    val `data`: Data,
    val message: String,
    val statusCode: Int
)