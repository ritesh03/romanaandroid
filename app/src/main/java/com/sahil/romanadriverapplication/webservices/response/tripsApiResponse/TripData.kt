package com.sahil.romanadriverapplication.webservices.response.tripsApiResponse

data class TripData(
    val DestinationAddress: DestinationAddress,
    val OriginAddress: OriginAddress,
    val _id: String,
    val accountName: AccountName,
    val accountOwnerName: String,
    val amount: Double,
    val tripRequest:Int,
    val Notes:List<Notes>,
    val confirmationNumber: String,
    val driverFirstName: String,
    val driverName: DriverName,
    val passengerCount: Int,
    val passengerName: PassengerName,
    val status: String,
    val tripDate: String,
    val tripNumber: Int,
    val tripStatus: Int,
    val tripTime: String
)