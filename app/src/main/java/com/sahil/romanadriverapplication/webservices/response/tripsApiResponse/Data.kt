package com.sahil.romanadriverapplication.webservices.response.tripsApiResponse

data class Data(
    val count: Int,
    val driverdata: List<Driverdata>,
    val tripData: List<TripData>
)