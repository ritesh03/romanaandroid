package com.sahil.romanadriverapplication.webservices.response.tripsApiResponse

data class AccountName(
    val _id: String,
    val name: String
)