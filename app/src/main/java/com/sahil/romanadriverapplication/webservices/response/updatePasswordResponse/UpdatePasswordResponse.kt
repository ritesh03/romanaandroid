package com.sahil.romanadriverapplication.webservices.response.updatePasswordResponse

data class UpdatePasswordResponse(
    val `data`: Data,
    val message: String,
    val statusCode: Int
)