package com.sahil.romanadriverapplication.webservices.response.tripsApiResponse

data class DriverName(
    val _id: String,
    val email: String,
    val firstName: String,
    val lastName: String
)