package com.sahil.romanadriverapplication.webservices.response.loginApiResponse

data class DataX(
    val Name: String,
    val lastName:String,
    val email:String,
    val id: String,
    val type: String
)