package com.sahil.romanadriverapplication.webservices.response.tripupadteresponse

data class Data(
    val DestinationAddress: DestinationAddress,
    val Notes: List<Note>,
    val OriginAddress: OriginAddress,
    val __v: Int,
    val _id: String,
    val accountName: String,
    val aids: List<Any>,
    val amount: Double,
    val confirmationNumber: String,
    val createdAt: String,
    val driverName: String,
    val importId: String,
    val isBlocked: Boolean,
    val isDeleted: Boolean,
    val passengerCount: Int,
    val passengerIds: List<String>,
    val passengerName: String,
    val passengers: List<Passenger>,
    val status: String,
    val tripDate: String,
    val tripNumber: Int,
    val tripRequest: String,
    val tripStatus: Int,
    val tripTime: String,
    val updatedAt: String
)