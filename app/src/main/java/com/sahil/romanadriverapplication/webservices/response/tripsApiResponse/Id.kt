package com.sahil.romanadriverapplication.webservices.response.tripsApiResponse

data class Id(
    val _id: String,
    val email: String,
    val firstName: String,
    val lastName: String
)