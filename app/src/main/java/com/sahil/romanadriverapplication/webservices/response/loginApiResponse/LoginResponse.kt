package com.sahil.romanadriverapplication.webservices.response.loginApiResponse

data class LoginResponse(
    val `data`: Data,
    val message: String,
    val statusCode: Int
)