package com.sahil.romanadriverapplication.webservices.response.tripsApiResponse

data class PassengerName(
    val _id: String,
    val firstName: String,
    val lastName: String
)