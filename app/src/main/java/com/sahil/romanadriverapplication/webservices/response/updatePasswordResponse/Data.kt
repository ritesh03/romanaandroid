package com.sahil.romanadriverapplication.webservices.response.updatePasswordResponse

data class Data(
    val __v: Int,
    val _id: String,
    val accountId: String,
    val createdAt: String,
    val email: String,
    val firstName: String,
    val insuranceExpireAt: String,
    val isBlocked: Boolean,
    val isDeleted: Boolean,
    val lastName: String,
    val licenseExpireAt: String,
    val licenseNumber: String,
    val password: String,
    val passwordResetToken: String,
    val phoneNo: Any,
    val showRevenue: Boolean,
    val updatedAt: String
)