package com.sahil.romanadriverapplication.webservices.retrofit

import android.content.Context
import com.sahil.romanadriverapplication.utils.AppConstants

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.readystatesoftware.chuck.ChuckInterceptor
object RetofitClient {
    lateinit var RETROFIT_CLIENT: ApiInterface
    fun get(): ApiInterface = RETROFIT_CLIENT
    fun setUpRetrofitClient(context: Context) {
        val client: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(ChuckInterceptor(context))
            .build()
        val retrofit = Retrofit.Builder()
            .baseUrl(AppConstants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
        RETROFIT_CLIENT = retrofit.create(ApiInterface::class.java)
    }
}