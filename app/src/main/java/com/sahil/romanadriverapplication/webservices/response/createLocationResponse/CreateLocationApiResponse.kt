package com.sahil.romanadriverapplication.webservices.response.createLocationResponse

data class CreateLocationApiResponse(
    val `data`: Data,
    val message: String,
    val statusCode: Int
)