package com.sahil.romanadriverapplication.webservices.retrofit

import com.sahil.romanadriverapplication.webservices.response.addnotesresponse.AddNotesResponse
import com.sahil.romanadriverapplication.webservices.response.createLocationResponse.CreateLocationApiResponse
import com.sahil.romanadriverapplication.webservices.response.loginApiResponse.LoginResponse
import com.sahil.romanadriverapplication.webservices.response.tripsApiResponse.TripsApiResponse
import com.sahil.romanadriverapplication.webservices.response.tripupadteresponse.TripUpdateApiResponse
import com.sahil.romanadriverapplication.webservices.response.updatePasswordResponse.UpdatePasswordResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @FormUrlEncoded
    @POST("driver/login")
    fun postApiLogIn(@FieldMap feilds: HashMap<String,String>): Call<LoginResponse>

    @GET("/admin/trip/triplist")
    fun apiAllListTrips(@Header("authorization") authorization: String,
                       @QueryMap map: HashMap<String, Any>): Call<TripsApiResponse>

    @FormUrlEncoded
    @PUT("/driver/updatePassword")
    fun apiUpdatePassword(@FieldMap map: HashMap<String,String>):Call<UpdatePasswordResponse>


    @FormUrlEncoded
    @POST("/driver/verify")
    fun apiForgotPassword(@FieldMap map: HashMap<String,String>):Call<ResponseBody>

    @FormUrlEncoded
    @POST("/admin/driver/create_Notes")
    fun apiCreteNote(@Header ("authorization")authorization: String,@FieldMap map: HashMap<String,String>):Call<AddNotesResponse>


    @FormUrlEncoded
    @PUT("/driver/createLocation")
    fun apiCreateDriverLocation(@Header ("authorization") authorization: String,@FieldMap map: HashMap<String,Any>):Call<CreateLocationApiResponse>

    @FormUrlEncoded
    @PUT("/trip/updatestatus")
    fun apiTripUpdate(@Header ("authorization")authorization: String,@FieldMap map:HashMap<String,Any>):Call<TripUpdateApiResponse>

}