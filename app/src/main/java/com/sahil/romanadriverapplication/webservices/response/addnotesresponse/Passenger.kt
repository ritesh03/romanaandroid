package com.sahil.romanadriverapplication.webservices.response.addnotesresponse

data class Passenger(
    val _id: String,
    val passengerId: String
)