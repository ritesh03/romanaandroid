package com.sahil.romanadriverapplication.webservices.response.tripupadteresponse

data class Passenger(
    val _id: String,
    val passengerId: String
)