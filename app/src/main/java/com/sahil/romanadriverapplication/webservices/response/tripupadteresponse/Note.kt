package com.sahil.romanadriverapplication.webservices.response.tripupadteresponse

data class Note(
    val _id: String,
    val createdAt: String,
    val note: String
)