package com.sahil.romanadriverapplication.webservices.response.loginApiResponse

data class Data(
    val access_token: String,
    val `data`: DataX
)