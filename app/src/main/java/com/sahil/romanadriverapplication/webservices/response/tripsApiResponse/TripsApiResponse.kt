package com.sahil.romanadriverapplication.webservices.response.tripsApiResponse

data class TripsApiResponse(
    val `data`: Data,
    val message: String,
    val statusCode: Int
)